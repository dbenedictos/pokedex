import 'package:flutter/material.dart';
import 'color.dart';

final kDarkTheme = ThemeData.dark().copyWith(
  brightness: Brightness.dark,
  primaryColor: kBackgroundUIColorDark,
  primaryIconTheme: IconThemeData(
    color: Colors.white,
  ),
  textTheme: TextTheme(
    headline1: TextStyle(
      fontSize: 25,
      fontWeight: FontWeight.w900,
      color: Colors.white.withOpacity(1.0),
    ),
    headline2: TextStyle(
      fontSize: 15,
      color: Colors.white70,
      fontWeight: FontWeight.w600,
    ),
    headline3: TextStyle(
      fontSize: 15,
      fontWeight: FontWeight.w500,
    ),
  ),
  scaffoldBackgroundColor: kBackgroundUIColorDark,
  floatingActionButtonTheme: FloatingActionButtonThemeData(
    backgroundColor: kFloatingActionButtonColorDark,
  ),
  bottomSheetTheme: BottomSheetThemeData(
    backgroundColor: Colors.transparent,
  ),
);

final kLightTheme = ThemeData(
  brightness: Brightness.light,
  primaryColor: kBackgroundUIColor,
  primaryIconTheme: IconThemeData(
    color: Colors.black,
  ),
  textTheme: TextTheme(
    headline1: TextStyle(
      fontSize: 25,
      fontWeight: FontWeight.w900,
      color: Colors.black,
    ),
    headline2: TextStyle(
      fontSize: 15,
      color: Colors.black54,
      fontWeight: FontWeight.w600,
    ),
    headline3: TextStyle(
      fontSize: 15,
      fontWeight: FontWeight.w500,
    ),
  ),
  scaffoldBackgroundColor: kBackgroundUIColor,
  floatingActionButtonTheme: FloatingActionButtonThemeData(
    backgroundColor: kFloatingActionButtonColor,
  ),
  bottomSheetTheme: BottomSheetThemeData(
    backgroundColor: Colors.transparent,
  ),
);
