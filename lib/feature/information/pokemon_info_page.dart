import 'package:ffuf_flutter_architecture/ffuf_flutter_architecture.dart' hide LoadingWidget;
import 'package:flutter/material.dart';
import 'package:pokedex/apis/pokemonapi/models/type_model.dart';
import 'package:pokedex/classes/information_helper.dart';
import 'package:pokedex/classes/type_background_color.dart';
import 'package:pokedex/feature/widget/loading_widget.dart';
import 'package:pokedex/state/actions/pokemon_info_page_action.dart';
import 'package:pokedex/state/app_state.dart';
import 'package:pokedex/utilities/color.dart';
import 'widgets/upper_part.dart';
import 'widgets/bottom_part.dart';
import 'package:pokedex/models/union_page_state.dart';
import 'package:pokedex/utilities/extensions.dart';

class PokemonInfoPageVM extends BaseModel<AppState> {
  PokemonInfoPageVM();
  Map<String, dynamic> evolutionChain;

  UnionPageState<Map<String, dynamic>> pageState;

  PokemonInfoPageVM.build({
    @required this.pageState,
    @required this.evolutionChain,
  }) : super(equals: [
          pageState,
          evolutionChain ?? {},
        ]);

  @override
  BaseModel fromStore() => PokemonInfoPageVM.build(
        pageState: _getPageState(),
        evolutionChain: state.evolutionChain,
      );

  UnionPageState<Map<String, dynamic>> _getPageState() {
    if (state.wait.isWaitingFor(pokemonInfoLoadingKey)) {
      return UnionPageState.loading();
    } else if (state.pokemonInformation?.isNotEmpty == true) {
      return UnionPageState(state.pokemonInformation);
    } else {
      return UnionPageState.error('information page error');
    }
  }
}

class PokemonInfoPageConnector extends StatelessWidget with TypeBackgroundColor, InformationHelper {
  PokemonInfoPageConnector({
    @required this.name,
    @required this.url,
    this.type,
  });

  final String name;
  final String url;
  final Type type;

  @override
  Widget build(Object context) {
    final brightness = Theme.of(context).brightness;
    return StoreConnector<AppState, PokemonInfoPageVM>(
      model: PokemonInfoPageVM(),
      onInit: (store) => store.dispatch(GetPokemonInformation(url: url)),
      builder: (BuildContext context, PokemonInfoPageVM vm) {
        return vm.pageState.when(
          (pokemonInfo) => _PokemonInfoPageWidget(
            backgroundColor: type != null
                ? getBackgroundColor(
                    type: type.name,
                    brightness: brightness,
                  )
                : getBackgroundColor(
                    type: getTypes(pokemonInfo).first.name,
                    brightness: brightness,
                  ),
            pokemonInfo: pokemonInfo,
            evolutionChain: vm.evolutionChain,
          ),
          loading: () => LoadingWidget(),
          error: (errorMessage) => Center(child: Text(errorMessage)),
        );
      },
    );
  }
}

class _PokemonInfoPageWidget extends StatelessWidget {
  final Color backgroundColor;
  final Map<String, dynamic> evolutionChain;
  final Map<String, dynamic> pokemonInfo;

  const _PokemonInfoPageWidget({
    Key key,
    @required this.backgroundColor,
    @required this.evolutionChain,
    @required this.pokemonInfo,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackgroundUIColor,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: backgroundColor,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SafeArea(
        bottom: false,
        child: Stack(
          alignment: AlignmentDirectional.topCenter,
          children: [
            Column(
              children: [
                Expanded(
                  child: UpperPart(
                    pokemonInformation: pokemonInfo,
                    color: backgroundColor,
                  ),
                ),
                Expanded(
                  child: BottomPart(
                    color: backgroundColor,
                    pokemonInformation: pokemonInfo,
                    evolutionChain: evolutionChain,
                  ),
                ),
              ],
            ),
            Positioned(
              bottom: MediaQuery.of(context).size.height * 0.5,
              child: Image.network(
                pokemonInfo.imageUrl,
                width: 200,
                height: 200,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
