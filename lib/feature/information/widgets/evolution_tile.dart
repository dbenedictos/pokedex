import 'package:ffuf_flutter_architecture/ffuf_flutter_architecture.dart';
import 'package:flutter/material.dart';
import 'package:pokedex/apis/pokemonapi/models/pokemon_model.dart';

class EvolutionTile extends StatelessWidget {
  EvolutionTile({@required this.item});

  final List<Pokemon> item;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: item
            .map((pokemon) => Column(
                  children: [
                    Image.network(
                      pokemon.url,
                      width: (MediaQuery.of(context).size.width - 40) / 3,
                    ),
                    Text(
                      pokemon.name.capitalize(),
                      style: Theme.of(context).textTheme.headline3,
                    ),
                  ],
                ))
            .toList(),
      ),
    );
  }
}
