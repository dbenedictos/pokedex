import 'package:ffuf_flutter_architecture/ffuf_flutter_architecture.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pokedex/apis/pokemonapi/models/statistic_model.dart';
import 'base_stat_bar.dart';

class BaseStatTile extends StatelessWidget {
  BaseStatTile({@required this.item});

  final Statistic item;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 7.5),
      child: Row(
        children: [
          Container(
            height: 20,
            width: (MediaQuery.of(context).size.width - 40) * .25,
            child: Text(
              item.name == 'hp' ? item.name.toUpperCase() : item.name.capitalize(),
              style: Theme.of(context).textTheme.headline2,
            ),
          ),
          Container(
            height: 20,
            width: (MediaQuery.of(context).size.width - 40) * .12,
            child: Text(
              item.value.toString(),
              style: Theme.of(context).textTheme.headline3,
            ),
          ),
          Container(
            height: 20,
            width: (MediaQuery.of(context).size.width - 40) * .63,
            child: BaseStatBar(
              rawScore: item.value,
              barWidth: (MediaQuery.of(context).size.width - 40) * .63,
            ),
          ),
        ],
      ),
    );
  }
}
