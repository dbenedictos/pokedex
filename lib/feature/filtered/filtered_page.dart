import 'package:ffuf_flutter_architecture/ffuf_flutter_architecture.dart' hide LoadingWidget;
import 'package:flutter/material.dart';
import 'package:pokedex/feature/home/widgets/pokemon_tile_button.dart';
import 'package:pokedex/feature/widget/loading_widget.dart';
import 'package:pokedex/state/actions/filtered_page_action.dart';
import 'package:pokedex/state/app_state.dart';
import 'package:pokedex/utilities/extensions.dart';
import 'package:pokedex/apis/pokemonapi/models/pokemon_model.dart';
import 'package:pokedex/models/union_page_state.dart';

class _FilteredPageVM extends BaseModel<AppState> {
  _FilteredPageVM();
  _FilteredPageVM.build({@required this.pageState}) : super(equals: [pageState]);

  UnionPageState<List<Pokemon>> pageState;

  @override
  BaseModel fromStore() => _FilteredPageVM.build(pageState: _getPageState());

  UnionPageState<List<Pokemon>> _getPageState() {
    if (state.wait.isWaitingFor(filterLoadingKey)) {
      return UnionPageState.loading();
    } else if (state.filteredPokemonList?.isNotEmpty == true) {
      return UnionPageState(state.filteredPokemonList);
    } else {
      return UnionPageState.error('Filtered Pokemon Error');
    }
  }
}

class FilteredPageConnector extends StatelessWidget {
  FilteredPageConnector({
    @required this.type,
  });
  final String type;

  @override
  Widget build(Object context) {
    return StoreConnector<AppState, _FilteredPageVM>(
      model: _FilteredPageVM(),
      onInit: (store) {
        store.dispatchFuture(GetFilteredPokemons(type: type));
      },
      builder: (BuildContext context, _FilteredPageVM vm) {
        return vm.pageState.when(
          (pokemons) => _FilterPageWidget(
            pokemons: pokemons,
            type: type,
          ),
          loading: () => LoadingWidget(),
          error: (errorMessage) => Center(child: Text(errorMessage)),
        );
      },
    );
  }
}

class _FilterPageWidget extends StatelessWidget {
  final List<Pokemon> pokemons;
  final String type;

  const _FilterPageWidget({
    Key key,
    @required this.pokemons,
    @required this.type,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        bottom: false,
        child: CustomScrollView(
          slivers: [
            SliverAppBar(
              backgroundColor: Theme.of(context).primaryColor,
              primary: true,
              leading: GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Icon(
                  Icons.arrow_back,
                ),
              ),
              flexibleSpace: FlexibleSpaceBar(
                centerTitle: true,
                title: Text(
                  type.capitalize(),
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1,
                ),
              ),
              expandedHeight: 100,
            ),
            SliverPadding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              sliver: SliverGrid.count(
                crossAxisCount: 2,
                mainAxisSpacing: 10,
                crossAxisSpacing: 10,
                childAspectRatio: 5 / 4,
                children: List.generate(pokemons.length, (index) {
                  return PokemonTileButton(
                    key: ObjectKey(pokemons.elementAt(index)),
                    pokemon: pokemons.elementAt(index),
                    imageIndex: pokemons.elementAt(index).url.pokeID,
                  );
                }),
              ),
            )
          ],
        ),
      ),
    );
  }
}
