import 'package:ffuf_flutter_architecture/ffuf_flutter_architecture.dart' hide LoadingWidget;
import 'package:flutter/material.dart';
import 'package:pokedex/apis/pokemonapi/models/pokemon_model.dart';
import 'package:pokedex/feature/home/widgets/my_floating_action_button.dart';
import 'package:pokedex/feature/home/widgets/pokemon_tile_button.dart';
import 'package:pokedex/feature/widget/loading_widget.dart';
import 'package:pokedex/models/union_page_state.dart';
import 'package:pokedex/state/actions/home_page_action.dart';
import 'package:pokedex/state/app_state.dart';
import 'package:pokedex/utilities/extensions.dart';
import 'package:pokedex/utilities/string_constants.dart';

import 'widgets/pokemon_search.dart';

class _HomePageVM extends BaseModel<AppState> {
  _HomePageVM();

  UnionPageState<List<Pokemon>> pageState;

  _HomePageVM.build({@required this.pageState}) : super(equals: [pageState]);

  @override
  BaseModel fromStore() => _HomePageVM.build(pageState: _getPageState());

  UnionPageState<List<Pokemon>> _getPageState() {
    if (state.wait.isWaitingFor(pokemonLoadingKey)) {
      return UnionPageState.loading();
    } else if (state.pokemonList?.isNotEmpty == true) {
      return UnionPageState(state.pokemonList);
    } else {
      return UnionPageState.error('Home Page Error Message');
    }
  }
}

class HomePageConnector extends StatelessWidget {
  @override
  Widget build(Object context) {
    return StoreConnector<AppState, _HomePageVM>(
      model: _HomePageVM(),
      onInit: (store) {
        store.dispatch(GetPokemons());
      },
      builder: (BuildContext context, _HomePageVM vm) {
        return vm.pageState.when(
          (pokemons) => _HomePageWidget(pokemons: pokemons),
          loading: () => LoadingWidget(),
          error: (errorMessage) => Center(child: Text(errorMessage)),
        );
      },
    );
  }
}

class _HomePageWidget extends StatelessWidget {
  final List<Pokemon> pokemons;

  const _HomePageWidget({
    Key key,
    @required this.pokemons,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final aspectRatio = screenSize.width / screenSize.height;
    return Scaffold(
      floatingActionButton: MyFloatingActionButton(),
      body: SafeArea(
        bottom: false,
        child: CustomScrollView(
          slivers: [
            SliverAppBar(
              automaticallyImplyLeading: false,
              backgroundColor: Theme.of(context).primaryColor,
              primary: true,
              flexibleSpace: FlexibleSpaceBar(
                centerTitle: false,
                titlePadding: EdgeInsets.only(left: 20, bottom: 20),
                title: Text(
                  pokedex,
                  textAlign: TextAlign.left,
                  style: Theme.of(context).textTheme.headline1,
                ),
              ),
              expandedHeight: 100,
              actions: [
                Padding(
                  padding: const EdgeInsets.only(right: 20),
                  child: GestureDetector(
                    onTap: () => showSearch(
                      context: context,
                      delegate: PokemonSearch(list: pokemons),
                    ),
                    child: Icon(
                      Icons.search,
                      size: 35,
                    ),
                  ),
                ),
              ],
            ),
            SliverPadding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              sliver: SliverGrid.count(
                crossAxisCount: aspectRatio <= .90 ? 2 : 3,
                mainAxisSpacing: 10,
                crossAxisSpacing: 10,
                childAspectRatio: 5 / 4,
                children: List.generate(
                    pokemons.length,
                    (index) => PokemonTileButton(
                          key: UniqueKey(),
                          pokemon: pokemons.elementAt(index),
                          imageIndex: pokemons.elementAt(index).url.pokeID,
                        )),
              ),
            )
          ],
        ),
      ),
    );
  }
}
