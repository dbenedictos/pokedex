// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'about_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_About _$_$_AboutFromJson(Map<String, dynamic> json) {
  return _$_About(
    name: json['name'] as String,
    value: json['value'] as String,
  );
}

Map<String, dynamic> _$_$_AboutToJson(_$_About instance) => <String, dynamic>{
      'name': instance.name,
      'value': instance.value,
    };
