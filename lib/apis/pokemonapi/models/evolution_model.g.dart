// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'evolution_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Evolution _$_$_EvolutionFromJson(Map<String, dynamic> json) {
  return _$_Evolution(
    base: json['base'] == null
        ? null
        : Pokemon.fromJson(json['base'] as Map<String, dynamic>),
    middle: (json['middle'] as List)
        ?.map((e) =>
            e == null ? null : Pokemon.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    last: (json['last'] as List)
        ?.map((e) =>
            e == null ? null : Pokemon.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$_$_EvolutionToJson(_$_Evolution instance) =>
    <String, dynamic>{
      'base': instance.base,
      'middle': instance.middle,
      'last': instance.last,
    };
