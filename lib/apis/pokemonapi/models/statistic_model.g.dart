// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'statistic_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Statistic _$_$_StatisticFromJson(Map<String, dynamic> json) {
  return _$_Statistic(
    name: json['name'] as String,
    value: json['value'] as int,
  );
}

Map<String, dynamic> _$_$_StatisticToJson(_$_Statistic instance) =>
    <String, dynamic>{
      'name': instance.name,
      'value': instance.value,
    };
