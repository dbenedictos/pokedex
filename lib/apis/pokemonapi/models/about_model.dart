import 'package:freezed_annotation/freezed_annotation.dart';

part 'about_model.freezed.dart';
part 'about_model.g.dart';

@freezed
abstract class About with _$About {
  factory About({
    @JsonKey(nullable: true, name: 'name') String name,
    @JsonKey(nullable: true, name: 'value') String value,
  }) = _About;

  factory About.fromJson(Map<String, dynamic> json) => _$AboutFromJson(json);
}
