import 'package:freezed_annotation/freezed_annotation.dart';

part 'pokemon_model.freezed.dart';
part 'pokemon_model.g.dart';

@freezed
abstract class Pokemon with _$Pokemon {
  factory Pokemon({
    @JsonKey(nullable: true, name: 'url') String url,
    @JsonKey(nullable: true, name: 'name') String name,
  }) = _Pokemon;

  factory Pokemon.fromJson(Map<String, dynamic> json) => _$PokemonFromJson(json);
}
