import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:pokedex/apis/pokemonapi/models/pokemon_model.dart';

part 'evolution_model.freezed.dart';
part 'evolution_model.g.dart';

@freezed
abstract class Evolution with _$Evolution {
  factory Evolution({
    @JsonKey(nullable: true, name: 'base') Pokemon base,
    @JsonKey(nullable: true, name: 'middle') List<Pokemon> middle,
    @JsonKey(nullable: true, name: 'last') List<Pokemon> last,
  }) = _Evolution;

  factory Evolution.fromJson(Map<String, dynamic> json) => _$EvolutionFromJson(json);
}
