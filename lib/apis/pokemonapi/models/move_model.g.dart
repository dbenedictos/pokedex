// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'move_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Move _$_$_MoveFromJson(Map<String, dynamic> json) {
  return _$_Move(
    name: json['name'] as String,
  );
}

Map<String, dynamic> _$_$_MoveToJson(_$_Move instance) => <String, dynamic>{
      'name': instance.name,
    };
