import 'package:freezed_annotation/freezed_annotation.dart';

part 'ability_model.freezed.dart';
part 'ability_model.g.dart';

@freezed
abstract class Ability with _$Ability {
  factory Ability({
    @JsonKey(nullable: true, name: 'name') String name,
    @JsonKey(nullable: true, name: 'type') String type,
  }) = _Ability;

  factory Ability.fromJson(Map<String, dynamic> json) => _$AbilityFromJson(json);
}
