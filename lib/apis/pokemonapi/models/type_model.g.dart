// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'type_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Type _$_$_TypeFromJson(Map<String, dynamic> json) {
  return _$_Type(
    name: json['name'] as String,
  );
}

Map<String, dynamic> _$_$_TypeToJson(_$_Type instance) => <String, dynamic>{
      'name': instance.name,
    };
