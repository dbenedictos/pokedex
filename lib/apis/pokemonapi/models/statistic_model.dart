import 'package:freezed_annotation/freezed_annotation.dart';

part 'statistic_model.freezed.dart';
part 'statistic_model.g.dart';

@freezed
abstract class Statistic with _$Statistic {
  factory Statistic({
    @JsonKey(nullable: true, name: 'name') String name,
    @JsonKey(nullable: true, name: 'value') int value,
  }) = _Statistic;

  factory Statistic.fromJson(Map<String, dynamic> json) => _$StatisticFromJson(json);
}
