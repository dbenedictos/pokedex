import 'package:ffuf_flutter_architecture/ffuf_flutter_architecture.dart';
import 'package:pokedex/state/app_state.dart';

abstract class LoadingAction extends ReduxAction<AppState> {
  LoadingAction({this.actionKey});

  final String actionKey;

  @override
  void before() => dispatch(WaitAction.add(actionKey));

  @override
  void after() {
    dispatch(WaitAction.remove(actionKey));
  }
}
