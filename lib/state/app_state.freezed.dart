// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'app_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
AppState _$AppStateFromJson(Map<String, dynamic> json) {
  return _AppState.fromJson(json);
}

/// @nodoc
class _$AppStateTearOff {
  const _$AppStateTearOff();

// ignore: unused_element
  _AppState call(
      {@JsonKey(name: 'pokemonList', ignore: true)
          List<Pokemon> pokemonList,
      @JsonKey(name: 'pokemonInformation', ignore: true)
          Map<String, dynamic> pokemonInformation,
      @JsonKey(name: 'evolutionChain', ignore: true)
          Map<String, dynamic> evolutionChain,
      @JsonKey(name: 'filteredPokemonList', ignore: true)
          List<Pokemon> filteredPokemonList,
      @JsonKey(name: 'wait', ignore: true)
          Wait wait}) {
    return _AppState(
      pokemonList: pokemonList,
      pokemonInformation: pokemonInformation,
      evolutionChain: evolutionChain,
      filteredPokemonList: filteredPokemonList,
      wait: wait,
    );
  }

// ignore: unused_element
  AppState fromJson(Map<String, Object> json) {
    return AppState.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $AppState = _$AppStateTearOff();

/// @nodoc
mixin _$AppState {
  @JsonKey(name: 'pokemonList', ignore: true)
  List<Pokemon> get pokemonList;
  @JsonKey(name: 'pokemonInformation', ignore: true)
  Map<String, dynamic> get pokemonInformation;
  @JsonKey(name: 'evolutionChain', ignore: true)
  Map<String, dynamic> get evolutionChain;
  @JsonKey(name: 'filteredPokemonList', ignore: true)
  List<Pokemon> get filteredPokemonList;
  @JsonKey(name: 'wait', ignore: true)
  Wait get wait;

  Map<String, dynamic> toJson();
  @JsonKey(ignore: true)
  $AppStateCopyWith<AppState> get copyWith;
}

/// @nodoc
abstract class $AppStateCopyWith<$Res> {
  factory $AppStateCopyWith(AppState value, $Res Function(AppState) then) =
      _$AppStateCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'pokemonList', ignore: true)
          List<Pokemon> pokemonList,
      @JsonKey(name: 'pokemonInformation', ignore: true)
          Map<String, dynamic> pokemonInformation,
      @JsonKey(name: 'evolutionChain', ignore: true)
          Map<String, dynamic> evolutionChain,
      @JsonKey(name: 'filteredPokemonList', ignore: true)
          List<Pokemon> filteredPokemonList,
      @JsonKey(name: 'wait', ignore: true)
          Wait wait});
}

/// @nodoc
class _$AppStateCopyWithImpl<$Res> implements $AppStateCopyWith<$Res> {
  _$AppStateCopyWithImpl(this._value, this._then);

  final AppState _value;
  // ignore: unused_field
  final $Res Function(AppState) _then;

  @override
  $Res call({
    Object pokemonList = freezed,
    Object pokemonInformation = freezed,
    Object evolutionChain = freezed,
    Object filteredPokemonList = freezed,
    Object wait = freezed,
  }) {
    return _then(_value.copyWith(
      pokemonList: pokemonList == freezed
          ? _value.pokemonList
          : pokemonList as List<Pokemon>,
      pokemonInformation: pokemonInformation == freezed
          ? _value.pokemonInformation
          : pokemonInformation as Map<String, dynamic>,
      evolutionChain: evolutionChain == freezed
          ? _value.evolutionChain
          : evolutionChain as Map<String, dynamic>,
      filteredPokemonList: filteredPokemonList == freezed
          ? _value.filteredPokemonList
          : filteredPokemonList as List<Pokemon>,
      wait: wait == freezed ? _value.wait : wait as Wait,
    ));
  }
}

/// @nodoc
abstract class _$AppStateCopyWith<$Res> implements $AppStateCopyWith<$Res> {
  factory _$AppStateCopyWith(_AppState value, $Res Function(_AppState) then) =
      __$AppStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'pokemonList', ignore: true)
          List<Pokemon> pokemonList,
      @JsonKey(name: 'pokemonInformation', ignore: true)
          Map<String, dynamic> pokemonInformation,
      @JsonKey(name: 'evolutionChain', ignore: true)
          Map<String, dynamic> evolutionChain,
      @JsonKey(name: 'filteredPokemonList', ignore: true)
          List<Pokemon> filteredPokemonList,
      @JsonKey(name: 'wait', ignore: true)
          Wait wait});
}

/// @nodoc
class __$AppStateCopyWithImpl<$Res> extends _$AppStateCopyWithImpl<$Res>
    implements _$AppStateCopyWith<$Res> {
  __$AppStateCopyWithImpl(_AppState _value, $Res Function(_AppState) _then)
      : super(_value, (v) => _then(v as _AppState));

  @override
  _AppState get _value => super._value as _AppState;

  @override
  $Res call({
    Object pokemonList = freezed,
    Object pokemonInformation = freezed,
    Object evolutionChain = freezed,
    Object filteredPokemonList = freezed,
    Object wait = freezed,
  }) {
    return _then(_AppState(
      pokemonList: pokemonList == freezed
          ? _value.pokemonList
          : pokemonList as List<Pokemon>,
      pokemonInformation: pokemonInformation == freezed
          ? _value.pokemonInformation
          : pokemonInformation as Map<String, dynamic>,
      evolutionChain: evolutionChain == freezed
          ? _value.evolutionChain
          : evolutionChain as Map<String, dynamic>,
      filteredPokemonList: filteredPokemonList == freezed
          ? _value.filteredPokemonList
          : filteredPokemonList as List<Pokemon>,
      wait: wait == freezed ? _value.wait : wait as Wait,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_AppState implements _AppState {
  _$_AppState(
      {@JsonKey(name: 'pokemonList', ignore: true)
          this.pokemonList,
      @JsonKey(name: 'pokemonInformation', ignore: true)
          this.pokemonInformation,
      @JsonKey(name: 'evolutionChain', ignore: true)
          this.evolutionChain,
      @JsonKey(name: 'filteredPokemonList', ignore: true)
          this.filteredPokemonList,
      @JsonKey(name: 'wait', ignore: true)
          this.wait});

  factory _$_AppState.fromJson(Map<String, dynamic> json) =>
      _$_$_AppStateFromJson(json);

  @override
  @JsonKey(name: 'pokemonList', ignore: true)
  final List<Pokemon> pokemonList;
  @override
  @JsonKey(name: 'pokemonInformation', ignore: true)
  final Map<String, dynamic> pokemonInformation;
  @override
  @JsonKey(name: 'evolutionChain', ignore: true)
  final Map<String, dynamic> evolutionChain;
  @override
  @JsonKey(name: 'filteredPokemonList', ignore: true)
  final List<Pokemon> filteredPokemonList;
  @override
  @JsonKey(name: 'wait', ignore: true)
  final Wait wait;

  @override
  String toString() {
    return 'AppState(pokemonList: $pokemonList, pokemonInformation: $pokemonInformation, evolutionChain: $evolutionChain, filteredPokemonList: $filteredPokemonList, wait: $wait)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _AppState &&
            (identical(other.pokemonList, pokemonList) ||
                const DeepCollectionEquality()
                    .equals(other.pokemonList, pokemonList)) &&
            (identical(other.pokemonInformation, pokemonInformation) ||
                const DeepCollectionEquality()
                    .equals(other.pokemonInformation, pokemonInformation)) &&
            (identical(other.evolutionChain, evolutionChain) ||
                const DeepCollectionEquality()
                    .equals(other.evolutionChain, evolutionChain)) &&
            (identical(other.filteredPokemonList, filteredPokemonList) ||
                const DeepCollectionEquality()
                    .equals(other.filteredPokemonList, filteredPokemonList)) &&
            (identical(other.wait, wait) ||
                const DeepCollectionEquality().equals(other.wait, wait)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(pokemonList) ^
      const DeepCollectionEquality().hash(pokemonInformation) ^
      const DeepCollectionEquality().hash(evolutionChain) ^
      const DeepCollectionEquality().hash(filteredPokemonList) ^
      const DeepCollectionEquality().hash(wait);

  @JsonKey(ignore: true)
  @override
  _$AppStateCopyWith<_AppState> get copyWith =>
      __$AppStateCopyWithImpl<_AppState>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_AppStateToJson(this);
  }
}

abstract class _AppState implements AppState {
  factory _AppState(
      {@JsonKey(name: 'pokemonList', ignore: true)
          List<Pokemon> pokemonList,
      @JsonKey(name: 'pokemonInformation', ignore: true)
          Map<String, dynamic> pokemonInformation,
      @JsonKey(name: 'evolutionChain', ignore: true)
          Map<String, dynamic> evolutionChain,
      @JsonKey(name: 'filteredPokemonList', ignore: true)
          List<Pokemon> filteredPokemonList,
      @JsonKey(name: 'wait', ignore: true)
          Wait wait}) = _$_AppState;

  factory _AppState.fromJson(Map<String, dynamic> json) = _$_AppState.fromJson;

  @override
  @JsonKey(name: 'pokemonList', ignore: true)
  List<Pokemon> get pokemonList;
  @override
  @JsonKey(name: 'pokemonInformation', ignore: true)
  Map<String, dynamic> get pokemonInformation;
  @override
  @JsonKey(name: 'evolutionChain', ignore: true)
  Map<String, dynamic> get evolutionChain;
  @override
  @JsonKey(name: 'filteredPokemonList', ignore: true)
  List<Pokemon> get filteredPokemonList;
  @override
  @JsonKey(name: 'wait', ignore: true)
  Wait get wait;
  @override
  @JsonKey(ignore: true)
  _$AppStateCopyWith<_AppState> get copyWith;
}
